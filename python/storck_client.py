#!/usr/bin/env python3
import requests
import os

class StorckError(Exception):
    pass

class StorckWorkspaceClient:
    def __init__(self, storck_address=None, workspace_token=None, user_token=None):
        self.storck_address = storck_address
        if workspace_token is None:
            workspace_token = os.environ.get('STORCK_WORKSPACE_TOKEN')
        if user_token is None:
            user_token = os.environ.get('STORCK_USER_TOKEN')
        self.workspace = workspace_token
        self.user_token = user_token
        self._address_header = 'http://{}'.format(self.storck_address)
        self.file_endpoint = '{}/api/file'.format(self._address_header)
        self.user_token = None
        self.create_auth_header = lambda : {'Authorization': 'Token {}'.format(self.user_token)}

    def upload_str(self, data, db_path):
        if not isinstance(data, str) or not isinstance(db_path, str):
            raise ValueError
        send_data = {'file': data, 'path': db_path}
        content = requests.post(self.file_endpoint, params={'token': self.workspace}, files=send_data, headers=self.create_auth_header())
        if not content.status_code == 200:
            raise StorckError
        else:
            return True

    def upload_file(self, filepath, db_path):
        if not isinstance(filepath, str):
            raise ValueError
        with open(filepath, 'r') as f:
            data = f.read()
        return self.upload_str(data, db_path)

    def download_str(self, db_path):
        content = requests.get(
        request.get(self.file_endpoint, params={'path':db_path, 'token':self.workspace},
        stream=True,
        headers=self.create_auth_header()
        )
        if not content.status_code == 200:
            raise StorckError
        else:
            return True
        data = content.raw.read()
        return data

    def download_file(self, filepath, db_path):
        data = self.download_str(db_path)
        with open(filepath, 'w') as f:
            f.write(data)


if __name__ == "__main__":
    client = StorckWorkspaceClient(
        'localhost:8000',
        workspace_token = '33ee6712-1dd7-492f-bca8-f1b2137d31c1'
        user_token = '726ac81c2efd79bc66f39d820322ed9ef4c24e7d'
    )
    client.upload_str('Test text data', 'Tests/data/test.txt')
    client.upload_file('./local/file.txt', 'Tests/data/test.txt')
    data = client.download_str('Tests/data/test.txt')
    client.download_file('./local/file.txt', 'Tests/data/test.txt')
